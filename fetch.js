//curl https://api.weather.gov/points/39.7456,-97.0892
//curl https://api.weather.gov/gridpoints/TOP/31,80/forecast
//curl http://localhost:55555/points/39.7456,-97.0892
//curl http://localhost:55555/gridpoints/TOP/31,80/forecast

//The latitude and longitude of the area.
//const loc = "LAT,LON";
const loc = "42.4382,-83.1191";
//Number of days to show.
const numdays = 2;
//How often to pull new weather data.
const update = 3600;
//Time to wait before showing the next day's weather.
const cycle = 6;
//The URL of the inital API.
const api = "https://api.weather.gov/points/" + loc;



//Sleep for n seconds.
async function sleep(n) {
	return new Promise(resolve => setTimeout(resolve, n * 1000));
}


//Request a URL and return the data.
async function get(url) {
	return new Promise((resolve, reject) => {
		const req = new XMLHttpRequest();
		req.onload = () => { resolve(req.responseText); }
		req.onerror = () => { throw req.statusText; }
		req.open("GET", url, true); //true for async
		//req.setRequestHeader("Cache-Control", "no-cache");
		req.send();
	});
}


//Function to grab the forcast for the next few days.
async function getStation(dat) {
	//Get the list of stations from the data given.
	var json = JSON.parse(dat);
	var stations = await 
		get(json.properties.observationStations)
		.then((dat) => { return JSON.parse(dat).features });
	
	//Look for the nearest station.
	var nearest = [1000, ""];
	var target = [
		json.properties.relativeLocation.geometry.coordinates[0],
		json.properties.relativeLocation.geometry.coordinates[1]
	];
	for (var i = 0; i < stations.length; ++i) {
		//The station's coords.
		var c = [
			stations[i].geometry.coordinates[0],
			stations[i].geometry.coordinates[1]
		];
		
		//Station's distance from the target location.
		var d = Math.sqrt(
			Math.pow( (c[0] - target[0]), 2 ) +
			Math.pow( (c[1] - target[1]), 2 )
		);
		
		//Use this station if it's closer.
		if (nearest[0] > d) {
			nearest = [d, stations[i].id];
		}
	}
	
	return nearest[1];
}


async function getTemp(url) {
	return get(url)
		.then(dat => {
			var temp = JSON.parse(dat).properties.temperature.value;
			return "Now: " + Math.round(temp * (9 / 5) + 32) + "℉";
		})
}


async function main() {
	var ele = document.getElementById("weather");
	var url;
	
	try {
		url = await get(api)
			.then(json => { return getStation(json); })
			.then(base => { return base + "/observations/latest"; });
		
		getTemp(url).then(s => {ele.innerHTML = s});
		
		setInterval(() => {
			 getTemp(url).then(s => {ele.innerHTML = s});
		}, update * 1000);
	}
	catch(e) {
		console.error(e);
		ele.innerHTML = e.message;
		await sleep(10);
		location.reload(true);
	}
}


main();

